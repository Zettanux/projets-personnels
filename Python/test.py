
myBool = True
maString = "Trololo"

#Index commence à 0 en python
print("Ceci est un test. Texte de la String = " + maString)
print("maString[1] = " + maString[1])
print("maString[0:2] = " + maString[0:2])
print("maString[4:] = " + maString[4:])

#Opérations sur les chaines de caractères
maString.upper()
maString.lower()
maString.capitalize()
maString.find('l')
maString.count('l')
tableauString = maString.split('o')
print("tableauString[0] = " + tableauString[0])
print(maString.split("o"))

# Tests sur les chaines
maString.isalpha()
maString.isdigit()
maString.isalnum()
maString.isspace()

#Gestion des listes
maliste = ["un","deux","trois","quatre"]
maliste.append(5)
maliste.remove("un")
maliste2 = ["six"]
maliste.extend(maliste2)
print(maliste)

#Transtypage
a = 2
b = str(a)
c = float(a)
d = int(a)
e = bin(a)
f = hex(a)

#def de procédure
def maFonction():
    a = 3
    print(a)

maFonction()

#gérer les inputs
result = input()
#resultSecret = getpass()
print(result)

#Gérer structure conditonnelle
if a>0:
    print("SUP")
elif a<0:
    print("INF")
else:
    print("EQUAL")

for letter in maString:
    print(letter)
for elem in maliste:
    print(elem)

while a!=0:
    print("Lol")
    a = a-1
    if a==1:
        continue
    break

#Gestion fichier
mode = 'r+' # ou w, ou a(apend) en rajoutant b si on veut l'ouvrir en binaire : rb, wb, ab, + pour rajouter la lecture : w+, a+ etc.
#monfile = open("test.txt",mode) #OK mais pas sécure si ça plante.
with open("test.txt",mode) as monfichier:
    #contenu = monfichier.read()
    for line in monfichier.readlines():
        print(line)
    monfichier.write("Hello World")
    print("fin fichier")
    monfichier.close()

#libraires
#from math # importe tout
from math import sqrt
print(sqrt(82))

#classe objets
class maClasse:
    compteur = 0 # tout est public
    valeur = 5
    
    def _init_(self,param1,param2): #constructeur de copie, self = this
        maClasse.compteur += 1
        self.valeur = param1
    
    def _get_COMPTEUR(self): #ACCESSEUR de l'attribut "COMPTEUR" (nom normalisé)
        print(maClasse.compteur) #Ici, self.compteur fait un appel récursif ! car appel de la méthode elle même pour l'affichage ! 
        return str(maClasse.compteur)
    
    def _set_COMPTEUR(self, val):
        compteur = val
        if val == 0:
            print("Remise A Zero")

    compteur = property(_get_COMPTEUR,_set_COMPTEUR) #l'appel à compteur passera désormais par les accesseurs et les mutateurs.

monObjet = maClasse()
monObjet.compteur = 0
print(monObjet.compteur)

#Affichage du type
#print type(myBool)


